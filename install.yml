#!/usr/bin/env ansible-playbook

# Usage:
#   ./install.yml --tags currently --skip-tags doomemacs

# manual test (example):
#   docker buildx build -f Dockerfile_ubuntu --target test-bootstrap -t dotfiles . && docker container run -it --rm -v $PWD:/work dotfiles /work/install.yml --tags currently
# play with the tags to reduce test time

- name: 'setup of working environment (workstation)'
  hosts: localhost

  tasks:
  - name: 'setup dotfiles in HOME'
    ##############################
    tags:
    - dotfiles
    - dotbare
    - currently
    - minimal
    block:
    - name: 'dotfiles: installing dependencies'
      become: true
      package:
        name:
        - git
        - fzf
        state: present
    - name: 'dotfiles: installing (clone) dotbare' # https://github.com/kazhala/dotbare/tree/master#others
      ansible.builtin.git:
        repo: https://github.com/kazhala/dotbare.git
        dest: $HOME/.dotbare
        depth: 1
    - name: 'dotfiles: clone dotfiles repository'
      ansible.builtin.shell: $HOME/.dotbare/dotbare finit --url https://Peter_Gaus@bitbucket.org/Peter_Gaus/dotfiles.git
      args:
        creates: $HOME/.cfg
      register: dotbare_result
      changed_when: '"Migration completed" in dotbare_result.stdout'


  - name: 'Zsh (incl. Oh My Zsh)' # highly dependent on dotfiles (dotbare)
    #############################
    tags:
    - oh-my-zsh
    - currently
    - minimal
    block:
    - name: 'oh-my-zsh: installing Zsh and dependencies'
      become: true
      package:
        name:
        - zsh
        - git
        state: present
    - name: 'oh-my-zsh: install [Oh My Zsh](https://github.com/robbyrussell/oh-my-zsh#basic-installation)'
      ansible.builtin.shell: curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh | sh -s -
      args:
        creates: $HOME/.oh-my-zsh
    - name: 'oh-my-zsh: installation is overwriting .zshrc -> restore config version again'
      ansible.builtin.shell: test -x $HOME/.dotbare/dotbare && $HOME/.dotbare/dotbare restore .zshrc
      ignore_errors: true
    - name: 'oh-my-zsh: create link to dotbare repository (as custom plugin)'
      ansible.builtin.file:
        src: ../../../.dotbare
        dest: $HOME/.oh-my-zsh/custom/plugins/dotbare
        state: link
        force: yes
    - name: 'oh-my-zsh: installing additional packages required by Oh My Zsh plugins' # see [[file:.zshrc::plugins]]
      block:
      - name: 'oh-my-zsh: OS packages'
        become: true
        package:
          name:
          - autojump
          - thefuck # for [The Fuck](https://github.com/ohmyzsh/ohmyzsh/tree/master/plugins/thefuck)
      when: ansible_facts['distribution'] != "Alpine" # for a better approach see https://docs.ansible.com/ansible/latest/user_guide/playbooks_best_practices.html#handling-os-and-distro-differences


  - name: 'Spacemacs'
    #################
    tags:
    - spacemacs
    block:
    - name: 'spacemacs: installing Emacs and dependencies'
      become: true
      package:
        name:
        - emacs
        - git
        state: present
    - name: 'spacemacs: installing Spacemacs' # see [Spacemacs install](https://github.com/syl20bnr/spacemacs#install)
      ansible.builtin.git:
        dest: $HOME/.spacemacs.d
        repo: https://github.com/syl20bnr/spacemacs
        depth: 1
        version: develop
    - name: 'spacemacs: create softlink .emacs.d'
      ansible.builtin.file:
        src: .spacemacs.d
        dest: $HOME/.emacs.d
        state: link
    - name: 'spacemacs: installing journal layer' #see [spacemacs-journal](https://github.com/ruibinx/spacemacs-journal#install)
      ansible.builtin.git:
        dest: $HOME/.emacs.d/private/journal
        repo: https://github.com/borgnix/spacemacs-journal.git
        depth: 1


  - name: 'Source Code Pro fonts'
    #############################
    # FONT_HOME=~/.local/share/fonts
    tags:
    - fonts
    - currently
    block:
      - name: 'fonts: installing fontconfig'
        become: true
        package:
          name:
          - fontconfig
          - git
          state: present
      - name: 'fonts: installing Source Code Pro fonts to $FONT_HOME ...'
        ansible.builtin.git:
          dest: $HOME/.local/share/fonts/adobe-fonts/source-code-pro
          repo: https://github.com/adobe-fonts/source-code-pro.git
          depth: 1
      - name: 'fonts: update font cache'
        shell: fc-cache "$HOME/.local/share/fonts/adobe-fonts/source-code-pro"


  - name: 'Doom Emacs'
    ##################
    tags:
    - doomemacs
    - currently
    block:
    - name: 'doomemacs: installing Emacs and dependencies'
      become: true
      package:
        name:
        - emacs
        - git
        state: present
    - name: 'doomemacs: installing Doom Emacs' # see https://github.com/doomemacs/doomemacs/tree/develop?tab=readme-ov-file#install
      ansible.builtin.git:
        repo: https://github.com/hlissner/doom-emacs
        dest: $HOME/.emacs.doom.d
        depth: 1
    - name: 'doomemacs: create softlink .emacs.d'
      ansible.builtin.file:
        src: .emacs.doom.d
        dest: $HOME/.emacs.d
        state: link
    - name: 'doomemacs: install: calling doom'
      ansible.builtin.shell: $HOME/.emacs.d/bin/doom install --no-env
      # args:
      #   creates: $HOME/.doom.d
      register: doom_install_result
      changed_when: '"✓ Installed" in doom_install_result.stdout'
    - name: 'doomemacs: link executable'
      block:
      - name: 'doomemacs: ensure .local/bin'
        ansible.builtin.file:
          path: $HOME/.local/bin
          state: directory
      - name: 'doomemacs: create link to doom'
        ansible.builtin.file:
          src: ../../.emacs.d/bin/doom
          dest: $HOME/.local/bin/doom
          state: link


  - name: 'Vivaldi'
    ###############
    tags:
    - vivaldi
    - currently
    block:
    - name: 'vivaldi: install'
      become: true
      ansible.builtin.apt:
        deb: https://downloads.vivaldi.com/stable/vivaldi-stable_6.5.3206.50-1_amd64.deb

  - name: 'Visual Studio Code'
    ##########################
    tags:
    - vscode
    - currently
    block:
    - name: 'vscode: install'
      become: true
      ansible.builtin.apt:
        deb: https://go.microsoft.com/fwlink/?LinkID=760868
