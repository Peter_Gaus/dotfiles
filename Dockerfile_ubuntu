ARG UBUNTU_VERSION=23.10
FROM ubuntu:${UBUNTU_VERSION} AS base

# add user
ARG USERNAME=user
RUN \
	apt-get update && \
	apt-get --assume-yes --no-install-recommends install sudo && \
	useradd --create-home $USERNAME && \
	usermod --append --groups sudo $USERNAME && \
	echo "$USERNAME ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/$USERNAME && \
	chmod 0440 /etc/sudoers.d/$USERNAME && \
	# cleanup apt
	apt-get clean && \
	rm -rf /var/lib/apt/lists/* /var/cache/apt/archives/*


# unminimize Ubuntu - inspired by [verteen/ubuntu-unminimize](https://hub.docker.com/r/verteen/ubuntu-unminimize/dockerfile)
# =================
RUN \
	apt-get update && \
	apt-get --assume-yes --no-install-recommends install \
		apt-utils \
		tzdata \
		locales && \
	# cleanup apt
	apt-get clean && \
	rm -rf /var/lib/apt/lists/* /var/cache/apt/archives/*
ARG TZ=Europe/Berlin
RUN \
	ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && \
	echo $TZ > /etc/timezone

ARG LOCALE=en_US.UTF-8
RUN \
	dpkg-reconfigure --frontend=noninteractive tzdata && \
	sed -i -e "s/# $LOCALE UTF-8/$LOCALE UTF-8/" /etc/locale.gen && \
    dpkg-reconfigure --frontend=noninteractive locales && \
    update-locale LANG=$LOCALE

ENV LANG $LOCALE
ENV LANGUAGE $LOCALE
ENV LC_ALL $LOCALE

RUN yes | unminimize

USER $USERNAME:$USERNAME
WORKDIR /home/$USERNAME


# TESTING

FROM base AS test-bootstrap
COPY install.sh /tmp/
# skip the Ansible call (last line)
# see [[file:install.sh::curl]]
RUN head --lines=-6 /tmp/install.sh | sh

FROM test-bootstrap AS test-playbook
# for testing the Ansible playbook
COPY install.yml /tmp/
RUN ansible-playbook /tmp/install.yml
CMD [ "zsh" ]
