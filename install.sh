#!/usr/bin/env bash
set -o errexit
set -o pipefail

# Usage:
#   curl -sSf "https://bitbucket.org/Peter_Gaus/dotfiles/raw/main/install.sh" | sh -s - --skip-tags fonts,doomemacs
#   curl --silent --show-error --fail https://bitbucket.org/Peter_Gaus/dotfiles/raw/main/install.sh | bash -s -

# detect if we are running inside Visual Studio Code
if [ "$REMOTE_CONTAINERS" = "true" ]; then

	echo "Running inside Visual Studio Code → going with Nix Home Manager…"

	# to test locally:
	# sudo nixos-firewall-tool open tcp 9418
	# git daemon --user-path=work/dotfiles --export-all --verbose
	# VSCode setting: "dotfiles.repository": "git://172.17.0.1/work/dotfiles"

	# ensure that USER is set
	if [ -z "${USER-}" ]; then
		USER="$(id --user --name)"
		readonly USER
		export USER
	fi

	sudo mkdir --parents /nix
	ls -la /nix
	sudo chown "$USER" /nix

	# check if xz command exists, if not install it
	if ! command -v xz &> /dev/null; then
		echo "xz could not be found, installing..."
		sudo apt-get update || true
		sudo apt-get install --no-install-recommends xz-utils
	fi

	# install Nix https://nixos.org/download/#nix-install-linux (single-user installation)
	sh <(curl -L https://nixos.org/nix/install) --no-daemon --yes
	# shellcheck source=/dev/null
	. "$HOME"/.nix-profile/etc/profile.d/nix.sh

	# backup an existing .zshrc
	if [ -f ~/.zshrc ]; then
		mv ~/.zshrc ~/.zshrc.chezmoi
	fi
	# install chezmoi https://www.chezmoi.io/docs/install/
	nix-env -i chezmoi
	chezmoi init --apply https://Peter_Gaus@bitbucket.org/Peter_Gaus/dotfiles.git
	# Single Command https://www.chezmoi.io/user-guide/daily-operations/#install-chezmoi-and-your-dotfiles-on-a-new-machine-with-a-single-command
	# sh -c "$(curl -fsLS get.chezmoi.io)" -- init --one-shot $GITHUB_USERNAME

	# install Home Manager https://nix-community.github.io/home-manager/index.xhtml#ch-nix-flakes
	nix run home-manager/release-24.11 -- init --switch

else

	# Bootstrap
	# prepare absolutely necessary dependencies
	# to bootstrap as fast as possible into ansible
	OS_ID=$(. /etc/os-release && echo "$ID")
	readonly OS_ID
	OS_VERSION_ID=$(. /etc/os-release && echo "$VERSION_ID")
	readonly OS_VERSION_ID
	case $OS_ID in
		alpine)
			sudo apk add --no-cache \
				curl \
				py3-pip \
				# for pip cryptography
				gcc \
				libffi-dev \
				musl-dev \
				python3-dev \
				# to allow a backslash in the previous line
		;;
		nixos)
			git clone --depth=1 https://github.com/kazhala/dotbare.git "$HOME"/.dotbare
			"$HOME"/.dotbare/dotbare finit --url https://Peter_Gaus@bitbucket.org/Peter_Gaus/dotfiles.git
			rm ~/.zshenv ~/.zshrc
			echo "Depending on how you installed Home Manager, run one of the following commands:"
			echo "	sudo nixos-rebuild switch"
			echo "	home-manager switch"
			exit 0
		;;
		ubuntu)
			# ensure that package list is updated inside VS Code
			if [ "$REMOTE_CONTAINERS" = "true" ]; then
				sudo apt-get update
			fi
			DEBIAN_FRONTEND=noninteractive sudo apt-get --assume-yes --no-install-recommends install \
				ansible \
				curl \
				# to allow a backslash in the previous line
		;;
		*)
			echo "This Linux distribution (with ID: $OS_ID) is NOT supported!"
			exit 1
	esac

	# install via ansible
	# PATH=~/.local/bin:$PATH
	# see [[file:Dockerfile_ubuntu::RUN ansible-playbook]]
	if [ "$REMOTE_CONTAINERS" = "true" ]; then
		echo "Running inside Visual Studio Code → install only minimal setup…"
		./install.yml --tags minimal "$@"
	else
		curl --silent --show-error --fail "https://bitbucket.org/Peter_Gaus/dotfiles/raw/main/install.yml" | ansible-playbook /dev/stdin --tags currently "$@"
	fi

fi
