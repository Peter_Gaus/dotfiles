# [TL;DR](https://en.wikipedia.org/wiki/Wikipedia:Too_long;_didn't_read)

# Installation
## from Scratch (recommended)
```
curl --silent --show-error --fail "https://bitbucket.org/Peter_Gaus/config/raw/master/install.sh" | sh -s - --skip-tags fonts,spacemacs
```

## Alternatvie with the help of Ansible
The _install.sh_ shell script is taking care of installing Ansible for you.  
If you have Ansible alreday installed on your system you could start the installation directly calling the Ansible playbook:
```
curl --silent --show-error --fail "https://bitbucket.org/Peter_Gaus/config/raw/master/install.yml" | ansible-playbook /dev/stdin --skip-tags fonts,spacemacs
```

## Dependecies
Supported Linux distribution:
- Ubuntu ≥ 18.04
- Alpine ≥ 3.12

As minimal requirement you need at least __curl__.

### Ubuntu 
```
apt-get update && \
apt-get --assume-yes --no-install-recommends install ca-certificates curl sudo
```


# config
Contains configurations (dotfiles) for the following applications:
- [awesomewm](https://awesomewm.org/)
- [Double Commander](https://doublecmd.sourceforge.io/)
- [GNU Emacs](https://www.gnu.org/software/emacs/)
- [Z shell](https://www.zsh.org/) (incl. setup of [Oh My Zsh](https://ohmyz.sh/))

# Features
* Automatic actions at each login (xinitrc).

# Known Bugs
* xinitrc is *not* executed during login.

# Wish-List
* Automatic installation of Emcas MELPA packages.
* Fundamental configuration of Vivaldi.
