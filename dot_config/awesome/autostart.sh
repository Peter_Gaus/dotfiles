#!/usr/bin/env bash

function run {
	if ! pgrep $1 ;
	then
		$@&
	fi
}

# run doublecmd
# run emacs
# run vivaldi

setxkbmap -option caps:escape us,us,de ,intl, grp:shifts_toggle
emacsclient --alternate-editor "" --create-frame --no-wait
